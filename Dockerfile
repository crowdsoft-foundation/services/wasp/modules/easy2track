FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

RUN apk add --no-cache jpeg-dev openjpeg-dev tiff-dev tk-dev

RUN pip install --upgrade pip

COPY src/requirements.txt /src/app/requirements.txt
RUN pip3 install -r /src/app/requirements.txt

COPY src /src

EXPOSE 8000

RUN chmod 777 /src/runApp.sh
RUN chmod +x /src/runApp.sh

CMD ["/src/runApp.sh"]
