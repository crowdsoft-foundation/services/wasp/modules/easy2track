from main import app
from planblick.httpserver import Server
from events.event_handler import event_handler
from pbglobal.pblib.amqp import client

from autorun import Autorun

if __name__ == '__main__':

    Autorun().post_deployment()

    name = "easy2track"
    queue = "easy2track"
    bindings = {}
    bindings['message_handler'] = ['addContact', 'addContactV2', 'contactAdded','removeContact', 'contactRemoved', 'addContactAddress', 'contactAddressAdded']
    callback = event_handler

    client.addConsumer(name, queue, bindings, callback)

    server = Server()
    server.start(flask_app=app, app_path="/", static_dir="/static", static_path="/docs", port=8000)
